module.exports = {
  'rules': { 
    'references-empty': [2, 'never'],
  },
  parserPreset: {
    parserOpts: {
      issuePrefixes: ['umaxhunt/uhcrawler#', "umaxhunt/umaxhunt#"]
    }
  },
};
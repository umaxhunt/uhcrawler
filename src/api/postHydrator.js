import Db from '../../data';

export default class PostHydrator {
  constructor(db) {
    if (db) {
      this.db = db;
    } else {
      this.db = new Db();
    }
  }

  static hydrateOne(post) {
    return {
      ph_id: post.id,
      createdAt: new Date(post.created_at),
      name: post.name,
      tagline: post.tagline,
      slug: post.slug,
      votes_count: post.votes_count,
      discussion_url: post.discussion_url,
      redirect_url: post.redirect_url,
      screenshot_url: post.screenshot_url['850px'],
      thumbnail_url: post.thumbnail.image_url,
      topics: post.topics.length ? post.topics.map(q=>q.name).join(',') : '',
    };
    // console.log(dbpost);
  }
}
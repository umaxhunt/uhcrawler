import CronService from '../services/cronService';
import Db from '../../data';

require('babel-polyfill');

const db = new Db();

async function handler (request, h) {
  const cron = new CronService(db);
  const result = await cron.do();
  if (result.err) {
    return h.response({err: result.err.message, stack: result.err.stack}).code(500);
  }
  return result;
}


export default {
  method: 'GET',
  path: '/cron',
  handler
}
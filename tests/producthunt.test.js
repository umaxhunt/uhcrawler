/* global it, describe, beforeEach, afterEach, expect */

import Api from '../src/api/producthunt';

const de = require('dotenv');

de.config();

describe('developer tests', () => {
  

  beforeEach(() => {
    // api = new Api(process.env.PH_TOKEN)
  });

  afterEach(() => {
    // api = undefined
  });

  test('fail with bad token', async () => {
    const api = new Api('qwe');
    let err;
    try {
      await api.get50latest()
    } catch (e) {
      err = e
    }
    expect(err).toBeDefined();
  });


  test('get response', async () => {
    const api = new Api(process.env.PH_TOKEN)
    const result = await api.get50latest();
    expect(result).toBeDefined();
    expect(result.posts).toBeDefined();
    expect(result.posts.length).toBe(50);
  });

  // test('getPeople', () => {
  //   expect(people.getPeople()).toEqual('Albert is 26');
  // });

  // test('getNewObject', () => {
  //   const newPeople = people.getNewObject();
  //   expect(newPeople).not.toBe(people.people);
  //   expect(newPeople).toEqual(people.people);    
  // });
});

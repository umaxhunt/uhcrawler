/* global it, describe, beforeEach, afterEach, expect */

import Db from '../data/';
import data from './fixtures/example_response_posts.json';
import PostHydrator from '../src/api/postHydrator'

describe('developer tests', () => {
  

  beforeEach(() => {
  });

  afterEach(() => {
  });

  test('get response', async () => {
    const db = new Db();
    const cnt = await db.repositories.posts.count();
    expect(cnt).toBeGreaterThanOrEqual(2);
    db.close();
  });
  
  test('hydrate object', async () => {
    const post = data.posts[3];
    // console.log(post);
    const model = PostHydrator.hydrateOne(post);
    expect(model.ph_id).toBe(135870);
    expect(model.createdAt).toMatchObject(new Date('2018-09-27T02:10:34.433-07:00')); // eslint-disable-line max-len
    expect(model.name).toBe('Activity Feed UI Kit React Native');
    expect(model.tagline).toBe('Add activity feeds to your app in 10 lines of code'); // eslint-disable-line max-len
    expect(model.slug).toBe('activity-feed-ui-kit-react-native');
    expect(model.votes_count).toBe(145);
    expect(model.discussion_url).toBe('https://www.producthunt.com/posts/activity-feed-ui-kit-react-native?utm_campaign=producthunt-api&utm_medium=api&utm_source=Application%3A+ohai+%28ID%3A+6451%29');
    expect(model.redirect_url).toBe('https://www.producthunt.com/r/d74f3ec3db7c1a/135870?app_id=6451');
    expect(model.screenshot_url).toBe('https://url2png.producthunt.com/v6/P5329C1FA0ECB6/ab6fdca7ddeaf9e66caaa3de974e7e2b/png/?max_width=850&url=https%3A%2F%2Fgetstream.io%2Freact-native-activity-feed%2F%3Fref%3Dproducthunt');
    expect(model.thumbnail_url).toBe('https://ph-files.imgix.net/a61a9ea5-e464-4195-9117-595f1a27bb6b?auto=format&fit=crop&h=570&w=430');
    expect(model.topics).toBe('Developer Tools,Tech');
  });

  // test('upsert posts', async () => {
  //   const db = new Db();
  //   const cronService = new CronService(db);

  //   const currentLastUpdate = await db.repositories.posts.max('updated_at');

  //   const cnt_changed = await cronService.do();

  //   const newLastUpdate = await db.repositories.posts.max('updated_at');

  //   expect(currentLastUpdate).toBeLessThan(newLastUpdate);



  //   db.close();
  // })

});

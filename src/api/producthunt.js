import fetch from 'node-fetch'

require('babel-polyfill');

export default class Producthunt {
  constructor(token){
    this.token = token;
  }

  async get50latest() {
    const response = await fetch('https://api.producthunt.com/v1/posts/all',{
      headers: { 
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': `Bearer ${this.token}`,
      },
    });

    if (response.ok !== true) {
      throw new Error('Unauthorized');
    }

    return response.json();
  }
}
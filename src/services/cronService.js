import Api from '../api/producthunt';
import PostHydrator from '../api/postHydrator';

require('babel-polyfill');

export default class CronService {

  constructor(db){
    this.db = db;
  }

  async do() {
    const api = new Api(process.env.PH_TOKEN)
    let apiResult;
    try {
      apiResult = await api.get50latest();
    } catch (err) {
      console.log(err);
      return {err}
    }
    
    const models = apiResult.posts.map(r => PostHydrator.hydrateOne(r));
    let result;
    try {
      result = await Promise.all(
        models.map(p=>this.db.repositories.posts.insertOrUpdate(p)));
    } catch (err) {
      console.log(err);
      return({err});
    }
    return result.filter(r => r === true).length;
  }
}
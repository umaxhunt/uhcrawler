// jest.config.js
module.exports = {
  verbose: true,
  testPathIgnorePatterns: ["<rootDir>/dist/", "<rootDir>/data/"]
};